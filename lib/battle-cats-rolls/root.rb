# frozen_string_literal: true

module BattleCatsRolls
  Root = File.expand_path("#{__dir__}/../..").freeze
  WebHost = ENV['WEB_HOST'].freeze
  SeekHost = ENV['SEEK_HOST'].freeze
end
